Um skeuomorphism was like Apple right?
But I think trends followed the web
Nice clean design
Much easier to achieve than ones with graphical complexity
Idk I think our design closely follows what we as a society value
Skeuomorphism was a period where we valued realism and making technology accessible
I think now we are obsessed with making things faster and flat design is a way to convey that message
As to whether a period of design will show up again, it just depends on our culture
Like look at the 1920s, and even the early 90s, you could see a design reflective of opulence because that was backed by the greed of the eras
And look at the obsession of making human like robots now
That is fairly skeuomorphic in its goal, to make robot interfaces more approachable. But that will soon change. It's just not economically sensible once we adopt them into our daily lives
kwang
skeuomorphism is useful because it provides an additional visual interface on top of the physical one.
flat design is useful because of quickness to parse information if you already somewhat know what to expect
i think that in regards to whether skeuomorphism will see a resurgence, it is strictly dependent if that is the culture that we are forced to live in
take for example vr technology
i think that in the next couple years, if we do see vr become more mass market
i think their interfaces will resemble much like smartphones
because it is a piece of design that we as a society have determined the rules for interaction and will be easy to take advantage of
but in a way, the debate is not strictly skeuomorphic vs flat
like take apple's lock screen for example
there is no reason they should need to you to "swipe right" to unlock
that piece of skeuomorphic design is plagiarized from an interface they popularized several years back
but in short, design is designed to serve people, not the other way around. so to see where design is headed i think we should instead ask where we as a society are headed.

